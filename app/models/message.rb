class Message
  include Mongoid::Document

  field :content
  field :created_at, type: Time
end