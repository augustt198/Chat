class MessagesController < ApplicationController

  def new_message
    if !params[:nickname] or params[:nickname].strip.empty?
      @nick = 'Anonymous'
    else
      @nick = params[:nickname].strip
    end
  end

end