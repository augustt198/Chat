# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BetterChat::Application.config.secret_key_base = 'ac2b99a76281892fa1ce31ba675170201c988b3de4888de37a442a63b38188f636ceff73fa10a7ee83f9b2e696b21ec3c64dc2faf743aa5f6f575169d7871365'
