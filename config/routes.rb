BetterChat::Application.routes.draw do
  
  root 'home#index'

  match '/new_message' => 'messages#new_message', via: [:post]

end
